//create table customers
	CREATE TABLE customers(
    		id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    		name varchar(255),
    		email varchar(255),
    		password varchar(255)
	);

//create table orders
	CREATE TABLE orders(
    		id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    		amount varchar(255),
    		customer_id int,
		FOREIGN KEY (customer_id) REFERENCES customers(id)
	);